/**
*@author Ridwan Olalere
*
*
*/
package strikeamatch

import (
	"strings"
	"fmt"
)

func letterPairs(str string) []string{
	numPairs := len(str) - 1
	pairs := make([]string, numPairs)
	
	arrString := strings.SplitN(str, "", len(str))	
	
	fmt.Println("arrString", arrString)

	for i := 0; i < numPairs; i++{
		lo := i
		hi := i + 1
		pair := arrString[lo] + arrString[hi]
		
		pairs[i] = pair
	}

	
	return pairs

}


func wordLetterPairs(str string) []string{
	var allPairs []string

	allWords := strings.Fields(str)
	
		
	for w := 0; w < len(allWords); w++{
		pairsInWord := letterPairs(allWords[w])
		for i := 0; i < len(pairsInWord); i++{
			allPairs = append(allPairs, pairsInWord[i])
		}
	}
	
	
	return allPairs
}

/**
*Returns the similarity ranking of two strings
*
*@param str1 the first string to compare
*@param str2 the second string to compare withg str1
*@return the similarity ranking ranking from 0 - 1
*
*/
func CompareStrings(str1 string, str2 string) float64{
	
	pairs1 := wordLetterPairs(strings.ToLower(str1))
	pairs2 := wordLetterPairs(strings.ToLower(str2))

	
	intersection := 0
	union := float64(len(pairs1) + len(pairs2))

	for i := 0; i < len(pairs1); i++{
		pair1 := pairs1[i]

		for j := 0; j < len(pairs2); j++{
			
			pair2 := pairs2[j]
			if pair1 == pair2 {
				intersection++
				pairs2 = append(pairs2[:j], pairs2[j+1:]...)
				break
			}
			
			
		}			
		
		
	}
	
	return (2.0 * float64(intersection))/union

}
